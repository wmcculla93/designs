# Web Designs

This is a collection of various web designs I have built over the past few years.
The designs are made using Photoshop and are all original work.

Two designs (Tile Source and Kindle Clothing) were built into fully fledged websites
(one being a CMS solution and the other being an e-Commerce solution, both scratch built)
while the other (Automate) was for a design challenge for a job application.
